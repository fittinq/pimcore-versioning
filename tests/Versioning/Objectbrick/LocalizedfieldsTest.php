<?php declare(strict_types=1);

namespace Tests\Fittinq\Pimcore\Versioning\Versioning\Objectbrick;

use Exception;
use PHPUnit\Framework\TestCase;
use Pimcore\Model\DataObject\Objectbrick\Data\TestBrick;
use Pimcore\Model\DataObject\TestObject;
use Tests\Fittinq\Pimcore\Versioning\Versioning\Configuration;

class LocalizedfieldsTest extends TestCase
{
    private TestObject $testObject;

    /**
     * @throws Exception
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->configuration = new Configuration();
        $this->changedFieldExtractor = $this->configuration->configure();
        $this->testObject = $this->configuration->setUpTestObject('Versioning/Objectbrick/Objectbrick');
    }

    /**
     * @throws Exception
     */
    public function test_returnTestBrickLocalizedfieldInputIfFieldChanges()
    {
        $objectbrick = new TestBrick($this->testObject);
        $this->testObject->getObjectbricks()->setTestBrick($objectbrick);

        $objectbrick->setLocalizedfieldInput("old", 'nl_NL');
        $this->testObject->save();
        $objectbrick->setLocalizedfieldInput("new", 'nl_NL');
        $this->testObject->save();

        $this->assertEquals(
            [
                'TestObject.TestBrick.localizedfieldInput.nl_NL',
                'TestObject.TestBrick.localizedfieldInput'
            ],
            $this->changedFieldExtractor->getLatestChanges($this->testObject)
        );
    }


    /**
     * @throws Exception
     */
    public function test_returnTestBrickLocalizedfieldTextareaIfFieldChanges()
    {
        $objectbrick = new TestBrick($this->testObject);
        $this->testObject->getObjectbricks()->setTestBrick($objectbrick);

        $objectbrick->setLocalizedfieldTextarea("old", 'nl_NL');
        $this->testObject->save();
        $objectbrick->setLocalizedfieldTextarea("new", 'nl_NL');
        $this->testObject->save();

        $this->assertEquals(
            [
                'TestObject.TestBrick.localizedfieldTextArea.nl_NL',
                'TestObject.TestBrick.localizedfieldTextArea'
            ],
            $this->changedFieldExtractor->getLatestChanges($this->testObject)
        );
    }
}
