<?php declare(strict_types=1);

namespace Tests\Fittinq\Pimcore\Versioning\Versioning\Objectbrick;

use Exception;
use PHPUnit\Framework\TestCase;
use Pimcore\Model\DataObject\Data\BlockElement;
use Pimcore\Model\DataObject\Data\QuantityValue;
use Pimcore\Model\DataObject\Objectbrick\Data\TestBrick;
use Pimcore\Model\DataObject\TestObject;
use Tests\Fittinq\Pimcore\Versioning\Versioning\Configuration;

class BlockTest extends TestCase
{
    private TestObject $testObject;
    private TestObject $oldRelation;
    private TestObject $newRelation;

    /**
     * @throws Exception
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->configuration = new Configuration();
        $this->changedFieldExtractor = $this->configuration->configure();
        $this->testObject = $this->configuration->setUpTestObject('Versioning/Objectbrick/Block');
        $this->oldRelation = $this->configuration->setUpTestObject('Versioning/DataObject/Relation');
        $this->oldRelation->save();
        $this->newRelation = $this->configuration->setUpTestObject('Versioning/DataObject/Relation');
        $this->newRelation->save();
    }

    /**
     * @throws Exception
     */
    public function test_returnTestBrickBlockIfInputChanges()
    {
        $objectbrick = new TestBrick($this->testObject);
        $this->testObject->getObjectbricks()->setTestBrick($objectbrick);

        $objectbrick->setBlock([["input" => new BlockElement('input', 'input', 'old')]]);
        $this->testObject->save();

        $objectbrick->setBlock([["input" => new BlockElement('input', 'input', 'new')]]);
        $this->testObject->save();

        $this->assertEquals(['TestObject.TestBrick.block'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }

    /**
     * @throws Exception
     */
    public function test_returnTestBrickBlockIfCheckboxChanges()
    {
        $objectbrick = new TestBrick($this->testObject);
        $this->testObject->getObjectbricks()->setTestBrick($objectbrick);

        $objectbrick->setBlock([["checkbox" => new BlockElement('checkbox', 'checkbox', true)]]);
        $this->testObject->save();
        $objectbrick->setBlock([["checkbox" => new BlockElement('checkbox', 'checkbox', false)]]);
        $this->testObject->save();

        $this->assertEquals(['TestObject.TestBrick.block'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }

    /**
     * @throws Exception
     */
    public function test_returnTestBrickBlockIfNumberChanges()
    {
        $objectbrick = new TestBrick($this->testObject);
        $this->testObject->getObjectbricks()->setTestBrick($objectbrick);

        $objectbrick->setBlock([["number" => new BlockElement('number', 'number', 1)]]);
        $this->testObject->save();

        $objectbrick->setBlock([["number" => new BlockElement('number', 'number', 2)]]);
        $this->testObject->save();

        $this->assertEquals(['TestObject.TestBrick.block'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }

    /**
     * @throws Exception
     */
    public function test_returnTestBrickBlockIfQuantityValueChanges()
    {
        $objectbrick = new TestBrick($this->testObject);
        $this->testObject->getObjectbricks()->setTestBrick($objectbrick);

        $objectbrick->setBlock([["quantityValue" => new BlockElement('quantityValue', 'quantityValue', new QuantityValue(10))]]);
        $this->testObject->save();

        $objectbrick->setBlock([["quantityValue" => new BlockElement('quantityValue', 'quantityValue', new QuantityValue(20))]]);
        $this->testObject->save();

        $this->assertEquals(['TestObject.TestBrick.block'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }

    /**
     * @throws Exception
     */
    public function test_returnTestBrickBlockIfSelectChanges()
    {
        $objectbrick = new TestBrick($this->testObject);
        $this->testObject->getObjectbricks()->setTestBrick($objectbrick);

        $objectbrick->setBlock([["select" => new BlockElement('select', 'select', 1)]]);
        $this->testObject->save();

        $objectbrick->setBlock([["select" => new BlockElement('select', 'select', 2)]]);
        $this->testObject->save();

        $this->assertEquals(['TestObject.TestBrick.block'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }

    /**
     * @throws Exception
     */
    public function test_returnTestBrickBlockIfMultiSelectChanges()
    {
        $objectbrick = new TestBrick($this->testObject);
        $this->testObject->getObjectbricks()->setTestBrick($objectbrick);

        $objectbrick->setBlock([["multiSelect" => new BlockElement('multiSelect', 'multiSelection', 1)]]);
        $this->testObject->save();

        $objectbrick->setBlock([["multiSelect" => new BlockElement('multiSelect', 'multiSelection', 2)]]);
        $this->testObject->save();

        $this->assertEquals(['TestObject.TestBrick.block'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }

    /**
     * @throws Exception
     */
    public function test_returnTestBrickBlockIfManyToOneRelationChanges()
    {
        $objectbrick = new TestBrick($this->testObject);
        $this->testObject->getObjectbricks()->setTestBrick($objectbrick);
        $objectbrick->setBlock([["manyToOneRelation" => new BlockElement('manyToOneRelation', 'manyToOneRelation', $this->oldRelation)]]);
        $this->testObject->save();

        $objectbrick->setBlock([["manyToOneRelation" => new BlockElement('manyToOneRelation', 'manyToOneRelation', $this->newRelation)]]);
        $this->testObject->save();

        $this->assertEquals(['TestObject.TestBrick.block'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }

    /**
     * @throws Exception
     */
    public function test_returnTestBrickBlockIfManyToManyRelationChanges()
    {
        $objectbrick = new TestBrick($this->testObject);
        $this->testObject->getObjectbricks()->setTestBrick($objectbrick);
        $objectbrick->setBlock([["manyToManyRelation" => new BlockElement('manyToManyRelation', 'manyToManyRelation', [$this->oldRelation])]]);
        $this->testObject->save();

        $objectbrick->setBlock([["manyToManyRelation" => new BlockElement('manyToManyRelation', 'manyToManyRelation', [$this->newRelation])]]);
        $this->testObject->save();

        $this->assertEquals(['TestObject.TestBrick.block'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }

    /**
     * @throws Exception
     */
    public function test_returnTestBrickBlockIfManyToManyObjectRelationChanges()
    {
        $objectbrick = new TestBrick($this->testObject);
        $this->testObject->getObjectbricks()->setTestBrick($objectbrick);
        $objectbrick->setBlock([["manyToManyObjectRelation" => new BlockElement('manyToManyObjectRelation', 'manyToManyObjectRelation', [$this->oldRelation])]]);
        $this->testObject->save();

        $objectbrick->setBlock([["manyToManyObjectRelation" => new BlockElement('manyToManyObjectRelation', 'manyToManyObjectRelation', [$this->newRelation])]]);
        $this->testObject->save();

        $this->assertEquals(['TestObject.TestBrick.block'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }

    /**
     * @throws Exception
     */
    public function test_returnTestBrickBlockIfInputChangesFromNull()
    {
        $objectbrick = new TestBrick($this->testObject);
        $this->testObject->getObjectbricks()->setTestBrick($objectbrick);

        $objectbrick->setBlock(null);
        $this->testObject->save();

        $objectbrick->setBlock([["input" => new BlockElement('input', 'input', 'new')]]);
        $this->testObject->save();

        $this->assertEquals(['TestObject.TestBrick.block'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }

    /**
     * @throws Exception
     */
    public function test_returnTestBrickBlockIfInputChangesToNull()
    {
        $objectbrick = new TestBrick($this->testObject);
        $this->testObject->getObjectbricks()->setTestBrick($objectbrick);

        $objectbrick->setBlock([["input" => new BlockElement('input', 'input', 'old')]]);
        $this->testObject->save();

        $objectbrick->setBlock(null);
        $this->testObject->save();

        $this->assertEquals(['TestObject.TestBrick.block'], $this->changedFieldExtractor->getLatestChanges($this->testObject));
    }
}
