<?php declare(strict_types=1);

namespace Tests\Fittinq\Pimcore\Versioning\Versioning;

use Exception;
use Fittinq\Pimcore\Versioning\Exceptions\NotEnoughVersionsException;
use PHPUnit\Framework\TestCase;
use Pimcore\Model\DataObject\TestObject;

class VersioningTest extends TestCase
{
    private TestObject $testObject;

    /**
     * @throws Exception
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->configuration = new Configuration();
        $this->changedFieldExtractor = $this->configuration->configure();
        $this->testObject = $this->configuration->setUpTestObject('Versioning/Versioning');
    }

    /**
     * @throws Exception
     */
    public function test_throwNotEnoughVersionsExceptionWhenObjectHasLessThanTwoVersions()
    {
        $this->testObject->save();
        $this->expectException(NotEnoughVersionsException::class);
        $this->changedFieldExtractor->getLatestChanges($this->testObject);
    }
}
