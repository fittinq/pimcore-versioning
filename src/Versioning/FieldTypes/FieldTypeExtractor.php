<?php declare(strict_types=1);

namespace Fittinq\Pimcore\Versioning\Versioning\FieldTypes;

use Pimcore\Model\DataObject\ClassDefinition\Data;
use Pimcore\Model\DataObject\Concrete;

interface FieldTypeExtractor
{
    /**
     * @return string[]
     */
    public function getChangedFields(Data $classDefinition, Concrete $lhs, Concrete $rhs): array;
}
