<?php declare(strict_types=1);

namespace Fittinq\Pimcore\Versioning\Versioning\FieldTypes\Objectbricks;

use Exception;
use Fittinq\Pimcore\Versioning\Versioning\FieldTypes\Block\BlockChangeIdentifier;
use Fittinq\Pimcore\Versioning\Versioning\FieldTypes\FieldTypeExtractor;
use Pimcore\Model\DataObject\ClassDefinition\Data;
use Pimcore\Model\DataObject\Concrete;
use Pimcore\Model\DataObject\Objectbrick\Data\AbstractData;
use Pimcore\Model\DataObject\Objectbrick\Definition;

class Objectbricks implements FieldTypeExtractor
{
    private BlockChangeIdentifier $blockChangeIdentifier;
    private LocalizedfieldChangeIdentifier $localizedfieldChangeIdentifier;

    public function __construct()
    {
        $this->blockChangeIdentifier = new BlockChangeIdentifier();
        $this->localizedfieldChangeIdentifier = new LocalizedfieldChangeIdentifier();
    }

    private array $changedFields;

    public function getChangedFields(Data $classDefinition, Concrete $lhs, Concrete $rhs): array
    {
        $this->changedFields = [];
        foreach ($classDefinition->getAllowedTypes() as $objectBrickKey) {
            try {
                $this->extractObjectbrick($classDefinition->getName(), $lhs, $rhs, $objectBrickKey);
            } catch (Exception $e) {
                // No need to do anything here, just continue the loop.
            }
        }

        return $this->changedFields;
    }

    /**
     * @param String $fieldName
     * @param Concrete $lhs
     * @param Concrete $rhs
     * @param $objectBrickKey
     *
     * @throws Exception
     */
    public function extractObjectbrick(string $fieldName, Concrete $lhs, Concrete $rhs, $objectBrickKey): void
    {
        $objectBrickDefinition = Definition::getByKey($objectBrickKey);
        $lhsObjectbrick = $this->getObjectBrick($fieldName, $objectBrickKey, $lhs);
        $rhsObjectbrick = $this->getObjectBrick($fieldName, $objectBrickKey, $rhs);

        $this->getChangedFieldsObjectBrick(
            $objectBrickKey,
            $objectBrickDefinition,
            $lhsObjectbrick,
            $rhsObjectbrick
        );
    }

    /**
     * @throws Exception
     */
    private function getObjectBrick(string $fieldName, string $objectBrickKey, Concrete $concrete)
    {
        $objectBricks = $concrete->get($fieldName);
        return $objectBricks->get($objectBrickKey);
    }

    /**
     * @throws Exception
     */
    private function getChangedFieldsObjectBrick(string $objectBrickKey, Definition $objectBrickDefinition, $lhsObjectbrick, $rhsObjectbrick): void
    {
        if (!$lhsObjectbrick && !$rhsObjectbrick) {
            return;
        }

        if (!$lhsObjectbrick || !$rhsObjectbrick) {
            /* This may seem strange, but we check whether one object brick is falsy and another isn't.
             * If this is the case the object brick has been either added or deleted.
             */
            $this->changedFields[] = $objectBrickKey;
            return;
        }

        $children = $objectBrickDefinition->getFieldDefinitions();

        foreach ($children as $child) {
            $this->extractChild($child, $objectBrickKey, $lhsObjectbrick, $rhsObjectbrick);
        }
    }

    private function extractChild(Data $child, string $objectBrickKey, AbstractData $lhsObjectbrickData, AbstractData $rhsObjectbrickData): void
    {
        $childName = $child->getName();

        match ($child->getFieldtype()){
            'block' => $this->extractChildBlock($objectBrickKey, $childName, $lhsObjectbrickData, $rhsObjectbrickData),
            'localizedfields' => $this->extractChildLocalizedfields($objectBrickKey, $childName, $lhsObjectbrickData, $rhsObjectbrickData),
            default => $this->extractChildDefault($child, $objectBrickKey, $lhsObjectbrickData, $rhsObjectbrickData)
        };
    }

    private function extractChildDefault(Data $child, string $objectBrickKey, AbstractData $lhsObjectbrickData, AbstractData $rhsObjectbrickData): void
    {
        $childName = $child->getName();

        $lhsValue = $child->getVersionPreview($lhsObjectbrickData->getValueForFieldName($childName));
        $rhsValue = $child->getVersionPreview($rhsObjectbrickData->getValueForFieldName($childName));

        if ($lhsValue !== $rhsValue) {
            $this->changedFields[] = $objectBrickKey . '.' . $childName;
        }
    }

    private function extractChildBlock(string $objectBrickKey, string $childName, AbstractData $lhsObjectbrickData, AbstractData $rhsObjectbrickData): void
    {
        $lhsValue = $lhsObjectbrickData->getValueForFieldName($childName);
        $rhsValue = $rhsObjectbrickData->getValueForFieldName($childName);
        /* This may seem strange, but with these two if statements we check whether one value is not an array and the
         * other isn't. If this is the case the block has been either added or deleted.
         */
        if (!is_array($lhsValue) && !is_array($rhsValue)) {
            return;
        }

        if (!is_array($lhsValue) || !is_array($rhsValue)) {
            $this->changedFields[] = $objectBrickKey . '.' . $childName;
            return;
        }

        if ($this->blockChangeIdentifier->hasChanged($lhsValue, $rhsValue)) {
            $this->changedFields[] = $objectBrickKey . '.' . $childName;
        }
    }

    private function extractChildLocalizedfields(string $objectBrickKey, ?string $childName, AbstractData $lhsObjectbrickData, AbstractData $rhsObjectbrickData)
    {
        $lhsValue = $lhsObjectbrickData->getValueForFieldName($childName)?->getItems();
        $rhsValue = $rhsObjectbrickData->getValueForFieldName($childName)?->getItems();
        /* This may seem strange, but with these two if statements we check whether one value is not an array and the
         * other isn't. If this is the case the block has been either added or deleted.
         */

        if ($lhsValue === $rhsValue){
            return;
        }

        if (!empty($changes = $this->localizedfieldChangeIdentifier->getChangedFields($lhsValue, $rhsValue))) {
            foreach ($changes as $changesByLocale){
                $this->handleByLocale($changesByLocale, $objectBrickKey);
            }
        }
    }

    private function handleByLocale(mixed $changesByLocale, string $objectBrickKey): void
    {
        foreach ($changesByLocale as $locale => $localizedfields) {
            foreach ($localizedfields as $localizedfield) {
                $this->changedFields[] = $objectBrickKey . '.' . $localizedfield . '.' . $locale;
                $this->changedFields[] = $objectBrickKey . '.' . $localizedfield;
            }
        }
    }
}
