<?php declare(strict_types=1);

namespace Fittinq\Pimcore\Versioning\Versioning\FieldTypes\DataObject;

use Fittinq\Pimcore\Versioning\Versioning\FieldTypes\FieldTypeExtractor;
use Pimcore\Model\DataObject\ClassDefinition\Data;
use Pimcore\Model\DataObject\Concrete;

class SimpleFieldTypeExtractor implements FieldTypeExtractor
{
    public function getChangedFields(Data $classDefinition, Concrete $lhs, Concrete $rhs): array
    {
        $fieldName = $classDefinition->getName();

        $lhsValue = $lhs->getValueForFieldName($fieldName);
        $rhsValue = $rhs->getValueForFieldName($fieldName);

        return $lhsValue !== $rhsValue ? [$fieldName] : [];
    }
}
