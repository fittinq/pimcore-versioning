<?php declare(strict_types=1);

namespace Fittinq\Pimcore\Versioning;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class PimcoreVersioningBundle extends Bundle
{
}
